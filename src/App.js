import { useState } from 'react';
import classes from './App.module.css';
import Modal from './Components/Modal';

function App() {
  const[showModal, setShowModal] = useState(false);

  const changeModal = () => {
    setShowModal(prev => !prev)
    console.log("Changing..")
  }

  return (
    <div className={classes.container}>
      <button onClick={changeModal} className={classes.button1}>I´m a button.</button>
      <button onClick={changeModal} className={classes.button2}>I´m a button.</button>
      <button onClick={changeModal} className={classes.button3}>I´m a button.</button>
      <button onClick={changeModal} className={classes.button4}>I´m a button.</button>
      <button onClick={changeModal} className={classes.button5}>I´m a button.</button>
      {showModal && <Modal zmena={changeModal}/>}
    </div>
  );
}

export default App;
