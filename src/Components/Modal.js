import classes from "./Modal.module.css";

const Modal = (props) => {
  return (
    <div>
      <div className={classes.background} />
      <div className={classes.modalWrapper}>
        <div className={classes.title}>
          <h1>Modal overlay</h1>
          <button onClick={() => props.zmena()}> X </button>
        </div>
        <div className={classes.body}>
          <h2>Are you sure?</h2>
        </div>
        <div className={classes.footer}>
          <button onClick={() => props.zmena()}> Si </button>
        </div>
      </div>
    </div>
  );
};

export default Modal;
